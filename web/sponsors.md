---
layout: page
title: "Sponsors"
permalink: /sponsors/
style: sponsors-page
---

# LAS Sponsors

Would you like to help make the Linux App Summit possible? Join us and [sponsor](https://linuxappsummit.org/sponsor/)!

## TRENDSETTER

### Canonical

<img class="sponsorlogo" src="/assets/canonical.png" alt="Canonical"/>

> Canonical is the publisher of Ubuntu, the OS for most public cloud workloads as well as the emerging categories of smart gateways, self-driving cars and advanced robots. Canonical provides enterprise security, support and services to commercial users of Ubuntu. Established in 2004, Canonical is a privately held company.
>
> [https://canonical.com](https://canonical.com)

## CHAMPION

### Codethink

<img class="sponsorlogo" src="/assets/codethink.svg" alt="Codethink"/>

> Codethink is an ethical, independent and versatile software services company, based in Manchester, UK. We are expert in the use of Open Source technologies for systems software engineering.
>
> Leveraging Open Source tools and techniques, we help customers to meet demanding challenges with unique innovations and accelerated productivity.
>
> We can analyse, develop, integrate, optimise and support bespoke solutions for your systems in IoT, automotive, finance and anything in between.
>
> [https://www.codethink.co.uk](https://www.codethink.co.uk)

## BACKER

### openSUSE

<img class="sponsorlogo" src="/assets/opensuse.png" alt="openSUSE"/>

> The openSUSE project is a worldwide effort that promotes the use of Linux everywhere. The openSUSE community develops and maintains a packaging and distribution infrastructure, which provides the foundation for the world’s most flexible and powerful Linux distribution.
>
> Our community works together in an open, transparent and friendly manner as part of the global Free and Open Source Software community.
>
> [https://opensuse.org](https://opensuse.org)

### MBition

<img class="sponsorlogo" src="/assets/mbition.svg" alt="MBition"/>

> At MBition, we strive to bring digital luxury to mobility users around the world. To accomplish this, we are redefining how software is developed within the automotive field as part of an international Mercedes-Benz software development network. As a 100% subsidiary of Mercedes-Benz Group AG, we develop and integrate the next generation of Mercedes-Benz in-car infotainment systems that are based on the Mercedes-Benz Operating System (MB.OS). But our contribution to MB.OS, the future of car software at Mercedes-Benz, does not stop here. To provide the most seamless & connected mobility experiences, we further engage in Advanced Driver Assistance Systems (ADAS) platform development and continuously improve our Mercedes me companion app for our customers.
>
> [https://mbition.io/](https://mbition.io/)

## SUPPORTER

### Fedora

<img class="sponsorlogo" src="/assets/fedora.svg" alt="Fedora"/>

> The Fedora Project is a community of people who create an innovative, free, and open source platform for hardware, clouds, and containers that enables software developers and community members to build tailored solutions for their users.
>
> [https://getfedora.org/](https://getfedora.org/)


## FRIEND

### Tuxedo Computers

<img class="sponsorlogo" src="/assets/tuxedo22.svg" alt="Tuxedo"/>

> TUXEDO Computers builds tailor-made hardware with Linux!
>
> For a wide variety of computers and notebooks - all of them individually
built und prepared - we are the place to go. From lightweight ultrabooks
up to full-fledged AI development stations TUXEDO covers every aspect of
modern Linux-based computing.
> In addition to that we provide customers with full service at no extra
cost: Self-programmed driver packages, tech support, fully automated
installation services and everything around our hardware offerings.
>
> [https://www.tuxedocomputers.com](https://www.tuxedocomputers.com)

### Slimbook

<img class="sponsorlogo" src="/assets/slimbook.svg" alt="Slimbook"/>

> SLIMBOOK has been in the computer manufacturing business since 2015, we build computers tailored for Linux environments and ship them worldwide.
>
> Delivering quality hardware with our own apps combined with an unrivaled tech support team to improve the end user experience is our main goal.
>
> We also firmly believe that not everything is about the hardware, SLIMBOOK has been involved from the beginning with the community. We started taking on small local projects aiming to bring the GNU/Linux ecosystem to everyone, partnering with state of the art Linux desktops like KDE among other OS’s. And of course > having our very own Linux academy, “Linux Center”, where free Linux / FOSS courses are imparted regularly for everyone. If you love Linux and need quality hardware to match, BE ONE OF US.
>
> [https://www.slimbook.es](https://www.slimbook.es)

