---
layout: page
title: "Location + Travel"
permalink: /local/
---

# Rovereto, Italy

## The Venue

The conference will take place at:

[Youth Center Smart Lab](https://www.cooperativasmart.it/index.php/smartlab/)
V.le Trento, 47/49, 38068 Rovereto TN, Italy

[![Alt text](/assets/venu-map-screenshot.png)](https://www.openstreetmap.org/export#map=17/45.90481/11.04259)
[© Openstreetmap Contributors](https://www.openstreetmap.org/copyright)

## Where to Stay

The following structures have different pricing ranges and offers.
All of them are reachable with public transportation and 30 minutes or less minutes walk from the venue.

- [Rovereto's Hostel](https://www.ostellorovereto.it/gb/)
- [Hotel Sant'Ilario](https://www.hotelsantilario.com/en/) (near the venue)
- [Ca' antica](https://www.bed-and-breakfast.it/en/trentino-alto-adige/caantica-rovereto/44530)
- [Hotel leon d'oro](https://www.hotelleondoro.it/en/index.html)

## Getting to Rovereto

### Visa restrictions
Citizens from the Schengen area countries require only a national ID card or a passport 
to enter Italy. 

Participants that require a visa, will be provided with an invitation letter from the conference organizing team.

To find out if you need to apply for a visa, visit to [italian Foreign Ministry website](https://vistoperitalia.esteri.it/home/en) and fill in the form.

### By plane
Rovereto is about 70 km from the airport of Verona (Valerio Catullo), 175 km from the airport of Venice (Marco Polo), 225 km from the airport of Milan-Linate and 240 km from the airport of Milan-Malpensa.

The recommended international airports to reach Rovereto are **Bologna**, **Milano Malpensa**, or **Verona**.

#### Bologna
By landing in the airport of Bologna (Marconi) you can take the "Marconi Express" train to reach the central train station in a few minutes.

#### Milano Malpensa
If you land in the airport of Milano Malpensa you can take the "Malpensa Express" right at the airport to reach the central train station in about one hour.

From either Bologna or Milano train stations you can then take high speed trains that will take you to Rovereto (see train section for details).

#### Verona
Another very close airport is the Verona airport (Catullo di Villafranca).
A shuttle bus service regularly (every 20 minutes) connects the Verona airport with the Verona Porta Nuova train station. From there it is possible to reach Rovereto by train. 

### By train

Rovereto train station is reachable by high speed trains from most north Italy cities.
It is possible to get the train tickets in most train station machines by paying either by cash or by card or even [buy them online](https://www.trenitalia.com/en.html) in advance.

#### Starting from Bologna central station
You can take a high speed train "Frecciargento" directly to Rovereto. This travel will take around 2 hours.

#### Starting from Milan central station
You can take a high speed train "Frecciargento" up to Verona Porta Nuova, where you need to change with another high speed or a rapid regional train to Rovereto. This travel will take around 3 hours.

#### Starting from Verona Porta Nuova central station
You can take a high speed or a rapid regional train directly to Rovereto. This travel will take around 40 minutes.

Refer to the [Trenitalia website](https://www.trenitalia.com/en.html) to plan your trip by train.

#### From Rovereto train station to the venue
To reach the venue (Smart Lab - V.le Trento, 47/49 38068 Rovereto TN) you can use the public bus of Rovereto.
Exiting the train station you have to use the pedestrian overpass that brings you in front of the "Iris Bar", then turn right and you will see the bus stop "Piazzale Orsi Stazione Fs" in front of a stained glass building. Here you can take the busses nr. 1; 2; B301. The ride should be of 10 minutes, and you have to stop at "S.Ilario Sav".
You can have more information on [the Trentino Trasporti website](
https://www.trentinotrasporti.it/en/calcola-percorso?placePartenza=Piazzale%20Orsi%20Stazione%20Fs,%20Rovereto,%20TN,%20Italia&placeArrivo=Youth%20Center%20-%20Smart%20Lab,%20Viale%20Trento,%20Rovereto,%20TN,%20Italia).

## COVID-19 restrictions

### Entering in Italy
In order to enter and travel in Italy from any other country (EU and non-EU) it is required to:

- complete the [Passenger Locator Form](https://app.euplf.eu/#/) **before departure** and submit it to the controlling personnel
- submit one of the COVID-19 green certificates or an equivalent certification (completion of vaccination series or recovery or swab test)

For more information see the section [Entry into Italy: Covid-19 green certificates, validity, equivalents and exceptions](https://www.salute.gov.it/portale/nuovocoronavirus/dettaglioContenutiNuovoCoronavirus.jsp?lingua=english&id=5412&area=nuovoCoronavirus&menu=vuoto&tab=1)

### Attending the conference
All participants accessing the venue must exhibit a COVID-19 green certificate or equivalent certification.

Face mask must be worn at all times in closed spaces (e.g. in conference rooms). Exceptions to this include the moments needed to drink and to eat.

For more detailed information can be found on [the website of the Italian Ministry of Health](https://www.salute.gov.it/portale/nuovocoronavirus/dettaglioContenutiNuovoCoronavirus.jsp?lingua=english&id=5412&area=nuovoCoronavirus&menu=vuoto)