---
layout: page
title: "Discuss"
permalink: /bofs/
---

# Call for Workshop & Break Out Session Days

If you are interested in hosting a BoF at this year's conference send us an [email](mailto:info@linuxappsummit.org).

In your email, please include the following information:
- BoF title
- Any content, description, or plans for the BoF
- Preferred date/time slot for their BoF
- The email address associated with your LAS2021 registration

You can find all of the currently available BoF time slots [here](https://conf.linuxappsummit.org/event/4/timetable/#all).

The full list of BoF time slots for this year are: TBD

<!--
1. Thursday 13 May @ 15:00 UTC
2. Thursday 13 May @ 15:30 UTC
3. Friday 14 May @ 14:30 UTC
4. Friday 14 May @ 15:00 UTC
5. Friday 14 May @ 15:30 UTC
6. Saturday 15 May @ 14:30 UTC
7. Saturday 15 May @ 15:00 UTC
8. Saturday 15 May @ 15:30 UTC
-->
